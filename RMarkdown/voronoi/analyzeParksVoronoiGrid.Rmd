---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

 
#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
 
library(glue)

library(ggplot2)
 
library(sf)

library(here) 

library(ggspatial)

library(units)

library(ggvoronoi)

library(patchwork)
```
 
---
---


#Parameters
```{r}

options(na.action = "na.exclude")

set.seed(123)

useQc <- F
topN <- 10

nCellsQc <- 100
nCellsMtl <- 100

csd <- ifelse(useQc,  "2423027",  "2466023")
city <- ifelse(useQc, "Quebec", "Montreal")

useOnlyLaCiTe <- T
 
Census2SfSp:::setCancensusOptions()

#Use  common proj for qc and mtl
epsg32198 <- "+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +datum=NAD83 +units=m +no_defs"
epsg4326 <- "+proj=longlat +datum=WGS84 +no_defs"
 
proj4StrEpsg32198 <-  "+init=epsg:32198"

```


---
---

#Read in the neigh
```{r}

shpNeigh <- readNeigh(useQc = useQc,
                      customProj =epsg32198 )

nS <- getNs(useQc)
idStr <- nS$getID()


shpNeigh %>% head()

```



#Read in parks
```{r}

if(useQc){
  if(useOnlyLaCiTe){
    results <- readQcLaCiteParks(shpNeigh = shpNeigh)
    idParks <- "name"
    cityStr <- "LaCiteLimoilou"
  }else{
    results <- readQcAllParks(shpNeigh = shpNeigh)
    idParks <- "NO_SEQUENC"
    cityStr <- "QuebecCity"
  }

 
}else{
  results <- readMtlParks(shpNeigh = shpNeigh)
  idParks <- "OBJECTID"
  cityStr <- "Montreal"
}

shpAllParksWithNeighMultNeigh <- results$shpAllParksWithNeighMultNeigh
shpParks <- results$shpParks

  
  
if(useQc){
  
  shpParks%<>% rename_at(vars(idParks), ~"name")
  shpAllParksWithNeighMultNeigh%<>% rename_at(vars(idParks), ~"name")
  
  shpParks%<>%mutate(name=as.factor(name))
  shpAllParksWithNeighMultNeigh%<>%mutate(name=as.factor(name))
  
  idParks <- "name"
}


shpParks %>% head
shpAllParksWithNeighMultNeigh %>% head
```

---
---


#Create a regular grid -> avoid the problem with large parks
```{r}

shpLarge <- shpParks %>% top_n(n = 10,wt=area)


getGridPark <- function(shpP, cellSizeInMeters=500){
  
  shpGrid <- data.frame()
  tryCatch({
    shpGrid <- st_make_grid(shpP, c(cellSizeInMeters,cellSizeInMeters)) %>% st_as_sf()
    shpGrid$name <- shpP$name
    shpGrid[[idParks]] <- shpP[[idParks]] 
  },error=function(e){
    print(glue("Fatal error at {shpP[[idParks]] } - {shpP$name} -{e}"))
  })

  
  return(shpGrid)
}

if(useQc){
  nCells <- nCellsQc
}else{
  nCells <- nCellsMtl
}

listPolyGrid <- map( 1:nrow(shpParks), 
        ~getGridPark(shpParks[.x, ],nCells))

#Make sure the grid size is correct
grid1 <- getGridPark( shpParks[1,], nCells)[1, ]
cellSize <- as.numeric( round( sqrt( set_units(   st_area(grid1) ,"m^2" ) ) ) )

shpGridParks <- do.call(rbind,listPolyGrid) %>% st_transform(crs=st_crs(shpNeigh))


```

##Plot the results
```{r}

shpGridParks %<>% mutate_at(vars(idParks), ~ as.factor(.))

pGrid <- ggplot() + 
  geom_sf(data=shpGridParks, aes_string(fill=idParks), lwd=0.1)+
  geom_sf(data=shpNeigh, linetype="dashed", alpha=0) + 
  theme_bw() + 
  theme(legend.position = "none") + 
  ggtitle("Discretizing large parks to account for difference in size") + 
  labs(subtitle = glue("{cityStr}"),
       caption = glue("Regular grid with cells of {cellSize}X{cellSize} meters.\nColor Indicates park"))

ggsave( here("Figures",city, glue("pMapRegularGrid_{cellSize}_{cityStr}.png")),
        pGrid,
        height = 10,
        width = 10)  
  
```

##Add in the regular park polygon side by side
```{r}

pGrid1 <- ggplot() + 
  geom_sf(data=shpGridParks %>% mutate_at(vars(idParks), ~ as.factor(.)), aes_string(fill=idParks), lwd=0.1)+
  geom_sf(data=shpNeigh, linetype="dashed", alpha=0) + 
  theme_bw() + 
  theme(legend.position = "none") + 
  labs(subtitle = "Discretized parks")

pParks1 <- ggplot() + 
  geom_sf(data=shpParks %>% mutate_at(vars(idParks), ~ as.factor(.)), aes_string(fill=idParks), lwd=0.1)+
  geom_sf(data=shpNeigh, linetype="dashed", alpha=0) + 
  theme_bw() + 
  theme(legend.position = "none") + 
  labs(subtitle ="Original park polygons") 

pBoth <- pGrid1 + 
  pParks1 +
  patchwork::plot_annotation(subtitle = glue("{cityStr}"),
                             caption = glue("Regular grid with cells of {cellSize}X{cellSize} meters.\nColor Indicates park")) 
  
   

ggsave( here("Figures",city, glue("pMapRegularGridBOTH_{cellSize}_{cityStr}.png")),
        pBoth,
        height = 10,
        width = 20)  
  


```

 
---
---


#Get the voronoi polygons with the center of the GRID (not the raw park polygon)
```{r}

shpParksCentroids <- shpGridParks %>% 
  st_set_crs(st_crs(shpNeigh)) %>% 
  SfSpHelpers::getCentroids() %>% 
  dplyr::select_at( vars(idParks, "name","lng","lat")) %>%  #Use OBJECTID for mtl as the pk sice multiple parcs can have the same name
  st_set_geometry(NULL)

shpVoronoiPoly  <- voronoi_polygon(shpParksCentroids,
                              x="lng",y="lat") %>% 
  st_as_sf()

shpVoronoiPoly %<>% st_set_crs( st_crs(shpNeigh) )
 
shpVoronoiPoly %>% head 
```

```{r}

pVoronoiNeigh <- ggplot() +
  geom_sf(data=shpVoronoiPoly ) + 
  geom_sf(data=shpParks %>% SfSpHelpers::getCentroids(),col="white" ,linetype="dotted",lwd=1) + 
  geom_sf(data=shpNeigh , aes_string(fill=idStr),alpha=0.2)  + #ond't add the blacka nd white theme : seems to remove some polygons
  theme(legend.position = "bottom") + 
  ggtitle("Voronoi partition with neighbourhoods")+
  labs(subtitle = glue("{cityStr}"),
       caption = "Neighbourhoods colored and park boundaries in white") + 
  scale_fill_discrete("Neighbourhood")


pVoronoiNeigh

ggsave( here("Figures",city, glue("mapVoronoiGridWithNeigh_{cityStr}.png")),
        pVoronoiNeigh ,
        height = 10,
        width = 10)

```

---
---

#Read in the cancensus data
```{r}
  
  shpDAWithVectors <- cancensus::get_census("CA16",
                                            level = "DA",
                                            regions =  list(CSD= csd ) ,
                                            labels = "short", 
                                            geo_format = "sf") %>%
  st_zm  #Seems to be a fuckup with z and m dimensions when reprojecting
 
  shpDAWithVectors %<>%  st_transform(crs=st_crs(shpNeigh))
  
  
  shpParksCvx <- shpParks %>%  
    st_union() %>% 
    st_convex_hull() #take the convex hull of all parks
  
  shpParksBbox <- shpParks %>%  
    st_union()  %>%
    SfSpHelpers::bbox_polygon() %>%  
    st_sfc %>%
    st_sf(crs=st_crs(shpNeigh)) 
 
  
  idxInter <- map_lgl(  st_intersects(shpDAWithVectors,shpParksBbox), ~!is_empty(.)) 
  stopifnot(sum(idxInter)>1)
  shpDAWithVectorsInter <- shpDAWithVectors[ idxInter, ]
  
  #Get the population in the cvx hull of the parks
  totalPopInit <- shpDAWithVectorsInter$Population %>% sum(na.rm=T)
  
  print(totalPopInit)
  
  plot(shpDAWithVectorsInter$geometry)
```

---
---


#Intersect the voronoi tesselation with stats can data to get pop per park
```{r}

#Watch out, the difference in population can be huge with shitty intersections - use spatial/area interpolation from sf directly
shpVoronoiPolyWithPop <-  st_interpolate_aw( 
                      x=shpDAWithVectors %>% dplyr::select(Population),
                      to=st_intersection(shpVoronoiPoly, st_buffer(st_union(shpNeigh),10)),  #intersect first => otherwise there is a huge 'edge effect' where polygons near the boundy are very large and therefore get a large pop because of spatial interpolation
                      extensive = T)



#Try to get back the corresponding park name
shpVoronoiPolyWithPopPark <- st_join(shpVoronoiPolyWithPop,
                                     shpVoronoiPoly, 
                                     largest=T) %>% 
  arrange(desc(Population))

sum(is.na(shpVoronoiPolyWithPopPark$name))


#Add the lagest neigh
shpVoronoiPolyWithPopPark %<>% st_join(shpNeigh %>% dplyr::select_at(vars(idStr)), largest=T)

#Check that the total population matches
totalPopInter <- shpVoronoiPolyWithPop$Population %>% sum(na.rm=T)
print(totalPopInit-totalPopInter)


#Missing Parks
ggplot( shpParks %>% filter_at(vars(idParks), ~!( . %in% shpVoronoiPolyWithPopPark[[idParks]])))+
  geom_sf() + 
  ggtitle("Missing parks - eliminated by the buffer")
```
 

```{r}

ggplot() + 
  geom_sf(data=shpVoronoiPolyWithPop, lwd=0.1 ,linetype="dashed") + 
  geom_sf(data=shpDAWithVectorsInter, fill="green",alpha=0.2, lwd=0.1) 

```


---
---

#Get the most populated parks
```{r}


shpPlot <- shpVoronoiPolyWithPopPark %>% 
  mutate_at(vars(idStr), ~str_replace(.,"Nord-Ouest.*", "Nord-Ouest de l'Ile")) %>% 
  rename_at(vars(idStr), ~"Neighbourhood") %>% 
  arrange_at(vars(idParks))  

topIDs <- shpVoronoiPolyWithPopPark %>% 
  st_set_geometry(NULL) %>% 
  group_by_at(vars(idParks)) %>% 
  summarise(totalPop=sum(Population)) %>% 
  top_n(n=topN, wt=totalPop) %>% 
  pull(idParks)

shpParksMostPopulous <- shpPlot %>% 
  filter_at(vars(idParks) ,~ . %in%  topIDs) %>% 
  mutate(name = fct_reorder(.f=name,.x=Population,.fun = sum,.desc = T)) %>% 
  arrange(desc(name))

  
totalPop <- shpParksMostPopulous$Population %>% sum
captionStr <- glue("{cityStr}. Top {topN} most `populated` parks\nTotal population served by parks: {round(totalPop,0)}")

shpParksMostPopulous %>% group_by(name) %>% summarise(totalPop = sum(Population))

```

#Neighbourhoods that touch
```{r}


shpNeighTouches <- shpNeigh[map_lgl( st_intersects(shpNeigh,shpParksMostPopulous ), ~!is_empty(.)) ,] %>% 
  rename_at(vars(idStr), ~"Neighbourhood")  %>% 
  mutate_at(vars ("Neighbourhood"), ~str_replace(.,"Nord-Ouest.*", "Nord-Ouest de l'Ile"))  %>% 
  SfSpHelpers::getCentroids()

#Get the actual parks - not the voronoi cell associated
shpParksMostPopulousNotVoronoi <- shpParks %>% filter_at(vars(idParks), ~. %in%  topIDs)


#Merge the polygons - if they span multiple neigh there will be multiple records for each pakr
shpParksMostPopulous$area <-  set_units( st_area(shpParksMostPopulous), "ha") %>% as.numeric()
shpParksMostPopulousMerged <- shpParksMostPopulous %>% 
  group_by(name) %>% 
  summarise(Population = sum(Population),
            totalArea= sum(area)) %>% 
  SfSpHelpers::getCentroids()


```

#Plot 

##Barplot
```{r}


pBarPopVoronoi <- ggplot(shpParksMostPopulous)+ 
  geom_col(aes(x=name,y=Population,fill=Neighbourhood)) + 
  geom_text(data=shpParksMostPopulousMerged, aes(x=name,y=Population+1000, label=round(totalArea,0))) +
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90),
         legend.box = "horizontal") + 
  xlab("") + 
  ggtitle("Parks serving the largest population") + 
  labs(subtitle = captionStr,
          caption = "Population catchment based on the voronoi partition\nGrid cell ACCOUNTS for difference in park size.\nSize (in ha) indicated.")  


(pBarPopVoronoi)

ggsave( here("Figures",city, "Bar" , "Grid", glue("pBarPopVoronoiGrid_{cellSize}_{topN}_{cityStr}.png")),
        pBarPopVoronoi ,
        height = 10,
        width = 10)


```

##Map

###With labels
```{r}

pMapParks <- ggplot()+ 
  geom_sf(data=shpNeighTouches, aes(col=Neighbourhood), alpha=0.2, linetype="dashed") + 
  geom_sf(data=shpParksMostPopulousMerged, aes(fill=name)) +
  geom_sf(data=shpParksMostPopulousNotVoronoi, fill="grey" , linetype="dotted") + 
  scale_fill_discrete(guide="none") + 
  theme_bw() + 
  theme(legend.position = "bottom") + 
  annotation_scale(location="br",width_hint=0.3 ) + 
  annotation_north_arrow(location="tl",
                         style = north_arrow_fancy_orienteering,
                         height = unit(0.5,"in")) + 
  ggtitle("Parks serving the largest population") + 
  labs(subtitle = captionStr,
              caption = "Population catchment based on the voronoi partition\nGrid cell ACCOUNTS for difference in park size.\nActual park in grey and catchment in color.")  +
    xlab("") + 
  ylab("")+ 
  guides(fill=F)
  
  
if(useQc){
  pMapParks <- pMapParks + 
    ggrepel::geom_label_repel(data=shpParksMostPopulousMerged, 
                            aes(x=lng,y=lat,label=name,fill=name) ,
                            alpha=0.5,
                            nudge_x=-1000, 
                            nudge_y=1500)   
}  else{
  pMapParks <- pMapParks + 
    ggrepel::geom_label_repel(data=shpParksMostPopulousMerged, 
                            aes(x=lng,y=lat,label=name,fill=name) ,
                            alpha=0.5,
                            nudge_x=-2000, 
                            nudge_y=2500)   
}
  



(pMapParks)

ggsave(here("Figures",city, "Maps", "Grid", glue("pMapPopVoronoiGridTop_{cellSize}_{topN}_{cityStr}.png")),
       pMapParks,
       width = 15,
       height = 10)

```


###Without labels - just colors
```{r}

pMapParksNoLabs <- ggplot()+ 
  geom_sf(data=shpParksMostPopulousMerged, aes(fill=name)) +
  geom_sf(data=shpParksMostPopulousNotVoronoi,   linetype="dotted") + 
  geom_sf(data=shpNeighTouches, aes(col=Neighbourhood), alpha=0.1, linetype="dashed") + 
  theme_bw() + 
  theme(legend.position = "bottom",
        legend.box = "vertical") + 
  #scale_fill_brewer(type="seq",palette = 3) +  #not enough colors in palette
  annotation_scale(location="br",width_hint=0.3 ) + 
  annotation_north_arrow(location="tl",
                         style = north_arrow_fancy_orienteering,
                         height = unit(0.5,"in")) + 
  ggtitle("Parks serving the largest population") + 
  labs(subtitle = captionStr,
        caption = "Population catchment based on the voronoi partition\nGrid cell ACCOUNTS for difference in park size.\nActual park in grey") +
  xlab("") + 
  ylab("") 


(pMapParksNoLabs)

ggsave(here("Figures",city, "Maps",  "Grid", glue("pMapPopVoronoiGridTopNoLabs_{cellSize}_{topN}_{cityStr}.png")),
       pMapParksNoLabs,
       width = 15,
       height = 10)


```

