---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
 
library(glue)

library(ggplot2)
 
library(sf)

library(here) 

library(ggspatial)

library(units)
```

---
---

#Parameters
```{r}

epsg32198 <- "+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"

```

---
---

#Read in the neigh
```{r}

shpNeigh <- readNeigh(useQc = T,
                      customProj=epsg32198)
shpNeigh %>% head

sum( set_units(st_area(shpNeigh),"ha") )

```

#Read in all individual shps
```{r, echo=F,message=FALSE}

pathShpsRead <- here("Data","GeoData","Parks","ManualParks", "individual")
listFileNames <- list.files(pathShpsRead,pattern=".*.shp$")
listFileNamesWithPath <-  file.path ( pathShpsRead, listFileNames)
listFileNamesNoShp <- str_match(listFileNames, ".*(?=.shp)")
  
listSf <- map(listFileNamesWithPath, st_read ) %>% setNames(listFileNamesNoShp)

listSf %>% length

```


---
---

#Merge them
```{r}

#Need the same number of cols - inspect problems
numCols <-  map_int(listSf, ncol)
idxErrors <- which(numCols !=2)
listSf[idxErrors]

for( k in idxErrors){
  listSf[[k]]  %<>% dplyr::select(id)
}



#crs <- listSf[[1]] %>% st_crs
crs <- st_crs(shpNeigh)
  
  
#There can be multiple polygons for each list
for(k in 1:length(listSf) ){
  
  tryCatch({

    shpNew <-  listSf[[k]]  %>% st_transform(crs=crs)
    shpNew$name <- names(listSf)[[k]]
       
    if(k==1){
      shpAllParks <- shpNew
    }else{
      shpAllParks <- rbind(shpAllParks, shpNew )
    }
  },error=function(e){
    print(glue("Fatal error with {k}th number - {e}"))
  })
  
}

#Fix the id var - the id var is the true unique key since there are names assigned to more than 1 row: multipolygons split into multiple rows
shpAllParks %<>% dplyr::select(-id)
shpAllParks$id <- 1:nrow(shpAllParks)

#Make the attr constant
st_agr(shpAllParks) <-  "constant"

shpAllParks %>% dim

shpAllParks %>% head
```


---
---


#Inspect neighbourhoods
```{r}


#We get a little more than required - vanier and somebeauport neigh
listTouches <- st_intersects(shpNeigh, shpAllParks)
shpNeighTouch <- shpNeigh[map_lgl(listTouches, ~!is_empty(.x) ), ]

ggplot(shpNeighTouch) + 
  geom_sf()


shpNeighLaCite <- shpNeigh %>%  filter( Arrondissement %in% c("La Cité", "Limoilou"))

listNeighLaCite <- shpNeighLaCite %>% pull(NOM)

ggplot(shpNeighLaCite) + 
  geom_sf()


totalArea <-  sum(set_units(st_area(shpNeighLaCite),"ha"))
```

#Check possible topology errors
```{r}

joinPark <- function(n){
  r <- tryCatch({
     a <-  st_join(shpAllParks[n,],shpNeigh, largest=T)
     r <- NA
  },error=function(e){
    print(glue("error at {n} - {e}"))
    r <- n
  })
  return(r)
}

listErrors <- list()
for(n in 1:nrow(shpAllParks)){
  listErrors <- rlist::list.append(listErrors, joinPark(n) )
}

idxErrors <- listErrors [ !is.na(listErrors) ] %>% unlist

assertthat::are_equal(nrow(shpAllParks[idxErrors, ]) , 0)

```

#Parks should lie within a single neighoburhood, but there might be some small overlap
```{r}

listNeighTouch <- st_intersects(shpAllParks , shpNeighLaCite)

idxMoreThanOneNeigh <- map_int(listNeighTouch, length) > 1

shpAllParks[idxMoreThanOneNeigh, ]

#Ok plot seems to suest everything is fine
ggplot() + 
  geom_sf(data=shpNeighLaCite, aes(col=NOM), alpha = 0.4) + 
  geom_sf(data=shpAllParks[idxMoreThanOneNeigh, ]) + 
  ggtitle("Parks touching multiple neighbourhoods")
```

#Add the neigh info
```{r}

#Assign to the largest neigh to make sure  eaxtly one neigh is matched
shpAllParksWithNeigh <- st_join(shpAllParks, shpNeighLaCite,largest=T)

#Check that we have the same number of sf
assertthat::are_equal(nrow(shpAllParksWithNeigh), nrow(shpAllParks) )

shpAllParksWithNeigh %>% head

```

---
---


#Inspect the area
```{r}
shpAllParks$area <- units::set_units( st_area(shpAllParks), "ha")

shpAllParksWithNeigh$area <- units::set_units( st_area(shpAllParks), "ha")

shpAllParksWithNeigh %>% head

shpAllParksWithNeigh$area %>% summary

shpAllParksWithNeigh %>% filter( as.numeric(area) == 0 )


assertthat::are_equal( nrow(shpAllParksWithNeigh %>% filter( as.numeric(area) == 0 )), 0)
```


---
---


#Ok cool stuff now write the shp
```{r}

pathShpsWrite<- here("Data","GeoData","Parks","ManualParks", "merged")
st_write(shpAllParksWithNeigh, dsn=file.path(pathShpsWrite,"laCiteLimoilouParks.shp"), delete_layer = TRUE)

pathShpsWriteOpen <- here("OpenData","laCiteLimoilouParks" )
st_write(shpAllParksWithNeigh, dsn=file.path(pathShpsWriteOpen,"laCiteLimoilouParks.shp"), delete_layer = TRUE)

```



