---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


#Librairies
```{r, message=F, echo=F, include=F,error=FALSE}

GeneralHelpers::loadAll()
 
library(glue)

library(ggplot2)
 
library(sf)

library(here) 

library(ggspatial)

library(units)

library(ggvoronoi)

library(tmap)
 
library(DBI)

```
 
#Parameters
```{r}

epsg32198 <- "+proj=lcc +lat_1=60 +lat_2=46 +lat_0=44 +lon_0=-68.5 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"
useOnlyLaCiTe <- F
```

---
---

#Read in the neigh
```{r}

shpNeigh <- readNeigh(useQc = T,
                      useOnlyLaCiTe,
                      customProj=epsg32198)
shpNeigh %>% head

shpNeigh %>% dim

totalArea <- sum( set_units(st_area(shpNeigh),"ha") ) %>% as.numeric()

totalArea

idStr <- "NOM"
listNeigh  <- shpNeigh$NOM
```

#Read parks
```{r}


if(useOnlyLaCiTe){
    results <- readQcLaCiteParks(shpNeigh = shpNeigh)
    titleStr <- "La Cite-Limoilou"
    nameStr <- "LaCiteLimoilou"
    idParks <- "name"
   
}else{
    results <- readQcAllParks(shpNeigh = shpNeigh)
    titleStr <- "Quebec City"
    nameStr <- "QuebecCity"
    idParks <- "NO_SEQUENC"
}


shpAllParksWithMultNeigh <- results$shpAllParksWithMultNeigh
shpAllParks <- results$shpParks

shpAllParks %>% head
shpAllParksWithMultNeigh %>% head


shpAllParks%<>% rename_at(vars(idParks), ~"name")
shpAllParksWithMultNeigh%<>% rename_at(vars(idParks), ~"name")

shpAllParks%<>%mutate(name=as.factor(name))
shpAllParksWithMultNeigh%<>%mutate(name=as.factor(name))

```


# Write table
```{r}



pwd <- keyringr::decrypt_gk_pw( glue("user charles"))

 
con <- DBI::dbConnect(RPostgres::Postgres(),
                          dbname = 'parks',
                          host='localhost',
                          user='charles',
                          password=pwd
  )

 
dbWriteTable(con,
             'shpAllParksWithMultNeighQc',
             shpAllParksWithMultNeigh %>% st_transform(crs=4326),
             overwrite=T)
dbWriteTable(con,
             'shpAllParksQc',
             shpAllParks %>% st_transform(crs=4326),
             overwrite=T)

```

---
---

#Inspect and plot
```{r}

ratioArea <- sum( shpAllParksWithMultNeigh$areaInter )/ sum(shpNeigh$neighArea )

capStr <- captionStr <- glue("{nrow(shpAllParks)} sites covering {round(sum(shpAllParks$area),0)} ha in {titleStr} out of {round(totalArea,0)} ha total\n{round(ratioArea*100,0)}% of the region is occupied by parks and cemetaries")


pParcs <- ggplot() + 
  geom_sf(data=shpNeigh, alpha = 0.2) + 
  geom_sf(data=shpAllParks, aes(col=name)) + 
  theme_bw() + 
  annotation_scale(location="br",width_hint=0.3 ) + 
  annotation_north_arrow(location="tl",
                         style = north_arrow_fancy_orienteering,
                         height = unit(0.5,"in")) + 
  ggtitle("Parks and cemetaries") + 
  labs(subtitle = capStr)

if(useOnlyLaCiTe){
  pParcs <- pParcs + theme(legend.position = "left") 
}else{
   pParcs <- pParcs + theme(legend.position = "none") 
}

(pParcs)

ggsave(here("Figures","Quebec", glue("map{nameStr}Parks.png")),
      pParcs,
      width=12,
      heigh=7)


```

#Accent over neighbourhoods
```{r}
#Small difference (~5 ha for the sum of park area), but we need to account for the fact that some of the parks span multiple boroughs
ratioAreaError <- sum(shpAllParksWithNeighMultNeighMultNeigh$area) / sum( units::set_units ( st_area( shpNeigh), "ha" ) )
ratioArea <- sum( units::set_units ( st_area( st_intersection(shpAllParksWithNeighMultNeighMultNeigh, shpNeigh) ), "ha") )/ sum( units::set_units ( st_area( shpNeigh), "ha" ) )

captionStr <- glue("{nrow(shpAllParksWithNeighMultNeighMultNeigh)} sites covering {round(sum(shpAllParksWithNeighMultNeighMultNeigh$area),0)} ha in La Cite-Limoilou borough out of {round(totalArea,0)} ha total\n{round(ratioArea*100,0)}% of the borough is occupied by parks and cemetaries")

pParcs2 <- ggplot() + 
  geom_sf(data=shpNeigh %>%   mutate(NOM=str_replace(NOM, "Vieux-Q.*", "Vieux-Qc")) , aes(col=NOM), alpha = 0.2) + 
  geom_sf(data=shpAllParks, col="green", fill="green", alpha=0.2) + 
  theme_bw() + 
    annotation_scale(location="br",width_hint=0.3 ) + 
  annotation_north_arrow(location="tl",
                         style = north_arrow_fancy_orienteering,
                         height = unit(0.5,"in")) + 
  ggtitle("Parks and cemetaries") + 
  labs(subtitle = captionStr) + 
  scale_color_discrete(name = "Neighbourhood")

if(useOnlyLaCiTe){
  pParcs2 <- pParcs2 + theme(legend.position = "bottom") 
}else{
   pParcs2 <- pParcs2 + theme(legend.position = "left") 
}


pParcs2

ggsave(here("Figures","Quebec", glue("map{nameStr}ParksNeigh.png")),
      pParcs2,
      width=10,
      heigh=7)


```


#Inspect area - by park
```{r}

shpPlot <- shpAllParksWithNeighMultNeighMultNeigh %>% 
  mutate(area=as.numeric(area)) %>% 
  mutate(name=as.factor(name)) %>% 
  mutate(name = fct_reorder(.f = name,.x=-area) ) %>% 
  mutate(NOM=str_replace(NOM, "Vieux-Q.*", "Vieux-Qc"))
 


pBar <- ggplot(shpPlot) + 
  geom_col(aes(x=name,y=area, fill=NOM)) + 
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90)) + 
  ylab("Area (in ha)") + 
  xlab("") +
  ggtitle("Parks and cemetaries by neighbourhood") + 
  labs(subtitle = titleStr,
       caption = "Only neighbourhood with largest overlap shown")+
  theme(legend.position = "left") + 
  ylim(c(0,300)) #keep the same scale for Mtl and Qc City


ggsave(here("Figures","Quebec",  glue("barPlot{nameStr}Parks.png")),
      pBar,
      width=10,
      heigh=7)

```


#Inspect area - by neigh



##Intersection to assign neigh
 
```{r}

#To adequately compute prop of territory, use st_intersection + adjust to ensure the total area remains the same
#This should increase the size of the dataset as parks get split into different neighbourhoods
shpAllParksWithMultNeigh <- st_intersection(shpAllParks  ,
                                                 shpNeigh   )  #Use the entire Qc region! Some parts of parks may fall outside La Cite borough

otherNeighVal <- "Other neighbourhood"

shpAllParksWithMultNeigh %<>% mutate(NOM = ifelse(!(NOM %in% listNeigh),otherNeighVal,NOM))

shpAllParksWithMultNeigh$areaInter <- units::set_units( st_area(shpAllParksWithMultNeigh), "ha")
shpAllParksWithMultNeigh %<>% mutate( areaInter= as.numeric(areaInter))

assertthat::are_equal(shpAllParksWithMultNeigh$areaInter, shpAllParksWithNeighMultNeighMultNeigh$area  )

#Not a huge difference
shpAllParksWithMultNeigh$areaInter %>% sum
shpAllParksWithNeighMultNeighMultNeigh$area %>% sum

```

#Plot by neigh
```{r}

topN <- 50

 


shpPlot <- shpAllParksWithNeighMultNeighMultNeigh %>% 
  group_by(name) %>% 
  mutate(areaParkInter=sum(areaInter)) %>%   #important to consider ony the area that is within the neigh shp! 
  mutate_at(vars(idStr), ~str_replace(.,"Nord-Oues.*","Nord-Ouest de l'Ile"))  %>% 
  ungroup %>% 
  arrange(desc(areaParkInter))
 
 
#Keep top parks only
topParks <- unique(shpPlot$name)[1:topN]
shpPlot %<>% filter(name %in% topParks)

#Reorder the levels - important to set the sum! fct (don't use the default median - this is not a box_plot!)
shpPlot %<>% mutate(name = fct_reorder(.f=name, .x=areaInter, .fun = sum, .desc = T))


 
pBar <- ggplot(shpPlot ) + 
  geom_col(aes_string(x="name",y="areaInter", fill=idStr) ) + 
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90)) + 
  ylab("Area (in ha)") + 
  xlab("") +
  ggtitle("Parks and cemetaries by neighbourhood") + 
  labs(subtitle = titleStr,
       caption = glue("Only {topN} largest green spaces shown")) + 
  guides(fill=guide_legend(title = "Neighbourhood")) +
  theme(legend.position = "left") + 
  ylim(c(0,300)) #keep the same scale for Mtl and Qc City


 

ggsave(here("Figures","Quebec", glue("barPlot{nameStr}ParksByNeigh.png")),
      pBar,
      width=10,
      heigh=7)

pBar


```

#Inspect the top parks since we have no name
```{r}

tmap_mode("view")


topParks3 <- unique(shpPlot$name)[1:3]
shpPlot3 <- shpPlot %>% filter(name %in% topParks3)

tm_shape(shpPlot3) +
    tm_polygons("name") + 
tm_shape(shpNeigh) +
    tm_polygons("NOM", alpha=0)
 




```


```{r}

shpAllParks %>% filter(name == 960419 ) 
shpAllParksWithNeighMultNeighMultNeigh%>% filter(name == 960419 ) 
```

#Group by
```{r}

shpAllParksByNeigh <- shpAllParksWithMultNeigh %>% 
  st_set_geometry(NULL) %>% 
  filter_at(vars(idStr), ~!.==otherNeighVal) %>%  #other neigh can represent a basket case of multiple neigh nd as such can have multiple neighAreas
  group_by_at(vars(idStr)) %>% 
  summarise(totalParcArea = sum(area),
            neighArea=unique(neighArea)) %>% 
  mutate(propAreaPark = as.numeric(totalParcArea/neighArea))  %>% 
  mutate_at(vars(idStr), ~str_replace(.,"Vieux-Q.*", "Vieux-Qc")) 

shpAllParksByNeigh %>% head

```

#Plot by neigh
```{r}

shpAllParksByNeigh %<>% mutate_at(vars(idStr), ~fct_reorder(.,propAreaPark,.desc =T))
shpAllParksByNeigh %<>% rename_at(vars(idStr), ~"Neighbourhood")

pBarByNeigh <- ggplot(shpAllParksByNeigh) + 
  geom_col(aes(x=Neighbourhood,y=propAreaPark), fill="darkgreen") + 
  theme_bw() + 
  theme(axis.text.x = element_text(angle=90)) + 
  ylab("Area (in ha)") + 
  xlab("") +
  ggtitle("Proportion of green space") + 
  labs(subtitle = titleStr)

ggsave(here("Figures","Quebec", glue("barPlot{nameStr}PropByNeigh.png")),
      pBarByNeigh,
      width=10,
      heigh=7)

pBarByNeigh
```